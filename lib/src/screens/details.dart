import 'package:flutter/material.dart';
import 'package:funeraria/src/models/places.dart';

class Details extends StatelessWidget {
  final Place place;
  Details(this.place);

  @override
  Widget build(BuildContext context) {
    var description = "\nTraslados dkfldñkdlñkldffffffffffffffffffffffffffffffffglñdkgñ";
    var amount = "\$300.00";
    if(place.place == "Servicios florales"){
      description = "\nfloralesdvndsklvndsfklvndfklvndfklvnsfdklvndflkvndflkvnflkvklfv";
      amount = "\$150.00";
    }
    else if(place.place == "Cremaciones"){
      description = "\nCremaciones dkvmdfslffffffffffffffffffffffffffffñvmdfklñsmvlf";
      amount = "\$800.00";
    }
    else if(place.place == "Esquelas"){
      description = "\nEsquelas vkmdflñdvdslñffffffffffffffffffffffffffffffffffvflñ";
      amount = "\$25.00";
    }
    else if(place.place == "Seguros"){
      description = "\nSeguros cdmkcsñldkdsfffffffffffffffffffffffffffffffflfkddklñ";
      amount = "\$25.00";
    }

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 300,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(1), bottomRight: Radius.circular(1),),
                  child: Image.asset('images/${place.image}', height: 400, width: double.infinity, fit: BoxFit.fill,),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: Row(
                    children: <Widget>[
                      //Icon(Icons.calendar_today, color: Colors.grey, size: 20,),
                      SizedBox(width: 5,),
                    ],
                  )
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RichText(
                    text: TextSpan(children: [
                      TextSpan(text: '${place.place} \n', style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600, color: Colors.black)),
                      TextSpan(text: description)
                    ], style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color: Colors.grey)),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 80,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(2), topRight: Radius.circular(2)),
                    color: Color(0xFF4139E4),

                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left:10),
                        child: RichText(
                          text: TextSpan(
                              children: [
                                TextSpan(text: 'Valor\n', style: TextStyle(fontSize: 18)),
                                TextSpan(text: amount, style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold))

                              ]
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right:10.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Solicitar', style: TextStyle(color: Color(0xFF4139E4), fontSize: 20),),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Positioned(
            top: 40,
            left: 10,
            child:  GestureDetector(
              onTap: (){
                Navigator.pop(context);
              },
              child: Align(
                alignment: Alignment.topLeft,
                child: Icon(Icons.arrow_back_ios, color: Colors.white,),
              ),
            ),
          )
        ],
      ),
    );
  }
}