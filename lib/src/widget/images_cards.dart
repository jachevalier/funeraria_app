import 'package:flutter/material.dart';
import 'package:funeraria/src/models/places.dart';

import 'image_card.dart';


class ImageCards extends StatefulWidget {
  @override
  _ImageCardsState createState() => _ImageCardsState();
}

class _ImageCardsState extends State<ImageCards> {
  List<Place> places = [
    Place(place: 'Servicios florales', image: 'florales.jpg'),
    Place(place: 'Cremaciones', image: 'cremaciones.jpg'),
    Place(place: 'Traslados', image: 'traslados.jpg'),
    Place(place: 'Esquelas', image: 'esquelas.jpg'),
    Place(place: 'Seguros', image: 'seguros.jpg'),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: places.length,
            itemBuilder: (_, index) {
              return ImageCard(
                place: places[index],
                name: places[index].place,
                picture: places[index].image,
              );
            }));
  }
}