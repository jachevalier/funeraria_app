import 'package:flutter/material.dart';
import 'package:funeraria/src/screens/welcome.dart';
import 'package:funeraria/src/screens/home.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  //home: Welcome(),
  home: Home(),
));
